// Demo: Dual CAN-BUS Shields, Data Pass-through
// Written by: Cory J. Fowler
// January 31st 2014
// This examples the ability of this library to support more than one MCP2515 based CAN interface.


#include <mcp_can.h>
#include <SPI.h>

unsigned long rxId;
byte len;
byte rxBuf[8];

byte txBuf0[] = {1,2,3,4,5,6,7,8};
byte txBuf1[] = {1,2,3,4,5,6,7,8};

MCP_CAN CAN0(10);                              // CAN0 interface usins CS on digital pin 10
MCP_CAN CAN1(9);                               // CAN1 interface using CS on digital pin 9
char msgString[128];
byte cnt = 0;
void setup()
{
  Serial.begin(115200);
  
  // init CAN0 bus, baudrate: 250k@16MHz
  int res = CAN0.begin(MCP_STDEXT, CAN_250KBPS, MCP_8MHZ);
  if(res == CAN_OK ){
  Serial.print("CAN0: Init OK!\r\n");
  CAN0.setMode(MCP_NORMAL);
  } else {
    Serial.print("CAN0: Init Fail!!!, res: \r\n");
    Serial.println(res);
  }
  
  // init CAN1 bus, baudrate: 250k@16MHz
 res = CAN1.begin(MCP_STDEXT, CAN_250KBPS, MCP_8MHZ);
  if(res == CAN_OK ){
  Serial.print("CAN1: Init OK!\r\n");
  CAN1.setMode(MCP_NORMAL);
  } else {
    Serial.print("CAN0: Init Fail!!!, res: \r\n");
    Serial.println(res);
  }
  
  //SPI.setClockDivider(SPI_CLOCK_DIV2);         // Set SPI to run at 8MHz (16MHz / 2 = 8 MHz)
  
  //CAN0.sendMsgBuf(1000000, 0, 8, txBuf0);
  
  CAN0.sendMsgBuf(1000000, 1, 8, txBuf0);
  CAN1.sendMsgBuf(1000001, 1, 8, txBuf1);
}

void loop(){ 
  cnt++;
  if(cnt > 250)
  {
    cnt = 0;
  }
  delay(500);
  if(!digitalRead(2)){                         // If pin 2 is low, read CAN0 receive buffer
    CAN0.readMsgBuf(&rxId, &len, rxBuf);       // Read data: len = data length, buf = data byte(s)

    Serial.print("\nCAN 0 ID: ");
    Serial.print(rxId - 0x80000000);
    Serial.print("\tLen: ");
    Serial.print(len);
    Serial.print("\t V: ");

     for(byte i = 0; i<len; i++){
        sprintf(msgString, " 0x%.2X", rxBuf[i]);
        Serial.print(msgString);
      }
      rxBuf[0] = cnt;
    CAN1.sendMsgBuf(rxId, 1, len, rxBuf);      // Immediately send message out CAN1 interface
  }
    delay(500);

  if(!digitalRead(3)){                         // If pin 3 is low, read CAN1 receive buffer
    CAN1.readMsgBuf(&rxId, &len, rxBuf);       // Read data: len = data length, buf = data byte(s)

    Serial.print("\nCAN 1 ID: ");
    Serial.print(rxId - 0x80000000);
    Serial.print("\tLen: ");
    Serial.print(len);
    Serial.print("\t V: ");

    for (byte i = 0; i < len; i++)
    {
      sprintf(msgString, " 0x%.2X", rxBuf[i]);
      Serial.print(msgString);
      }
            rxBuf[0] = cnt;

    CAN0.sendMsgBuf(rxId, 1, len, rxBuf);      // Immediately send message out CAN0 interface
  }
}

/*********************************************************************************************************
  END FILE
*********************************************************************************************************/
