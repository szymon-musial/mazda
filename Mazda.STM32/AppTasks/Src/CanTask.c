#include "can.h"
#include "FreeRTOS.h"
#include "semphr.h"
#include "AppTasks.h"
#include <stdbool.h>

#define CAN_FRAME_LENGTH 8

static CAN_RxHeaderTypeDef RxHeader;

App_AllocStaticSemaphore(DMA_CAN_CollectionCompletedSemaphore)

static uint8_t RxData[CAN_FRAME_LENGTH];
static char RxDataChar[CAN_FRAME_LENGTH * 2];

void HAL_CAN_RxFifo0MsgPendingCallback(CAN_HandleTypeDef *hcan)
{
  portBASE_TYPE xHigherPriorityTaskWoken = pdFALSE;

  HAL_CAN_GetRxMessage(hcan, CAN_RX_FIFO0, &RxHeader, RxData);
  xSemaphoreGiveFromISR(DMA_CAN_CollectionCompletedSemaphore, &xHigherPriorityTaskWoken);

  // SEGGER_SYSVIEW_RecordExitISR();
  portYIELD_FROM_ISR(xHigherPriorityTaskWoken);
}

void CANTaskPreparation()
{
  App_PrepareStaticSemaphore(DMA_CAN_CollectionCompletedSemaphore);
  
  HAL_CAN_Start(&hcan1);
  HAL_CAN_ActivateNotification(&hcan1, CAN_IT_RX_FIFO0_MSG_PENDING);
}

void CANTaskFunction(void *arg)
{
  while (true)
  {
    xSemaphoreTake(DMA_CAN_CollectionCompletedSemaphore, portMAX_DELAY);

    for (short i = 0; i < RxHeader.DLC; i++)
    {
      sprintf(&RxDataChar[i * 2], "%02X", RxData[i]);
    }
    SEGGER_SYSVIEW_PrintfHost("%s", RxDataChar);

    static char str[20];
    sprintf(str, "CAN/%lX", RxHeader.StdId);
    if(RxHeader.StdId == 0x200)
    {
      AppSenderFunction(str, RxDataChar, RxHeader.DLC * 2);
    }
  }
}
