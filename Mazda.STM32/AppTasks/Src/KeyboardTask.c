#include "AppTasks.h"
#include "FreeRTOS.h"
#include "semphr.h"
#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <stdbool.h>
#include "usb_device.h"
#include "usbd_hid.h"

#define QUEUE_LEN 5
App_AllocStaticQueue(KeyboardCmdQueue, 5, MQTTIncomingMessage_t)

void KeyboardTaskPreparation()
{
    App_PrepareStaticQueue(KeyboardCmdQueue);
}

void SendChar(const char *chars, uint16_t len);

void KeyboardTaskFunction(void *arg)
{
    MX_USB_DEVICE_Init();
    
    MQTTIncomingMessage_t message;

    while (true)
    {
        //vTaskDelay(500 / portTICK_PERIOD_MS);
        if (xQueueReceive(KeyboardCmdQueue, &message, portMAX_DELAY) == pdTRUE)
        {
            SendChar(message.data, message.len);
        }
    }
}

void KeyboardIncomingFunction(const char *data, uint16_t len)
{
    MQTTIncomingMessage_t message;
    message.data = data;
    message.len = len;

    xQueueSend(KeyboardCmdQueue, &message, portMAX_DELAY);
}

#define KeyPressTime 20

static uint8_t HID_buffer[8] = {0};
extern USBD_HandleTypeDef hUsbDeviceFS;

void USB_Keyboard_SendChar()
{
    // press keys
    USBD_HID_SendReport(&hUsbDeviceFS, HID_buffer, 8);

    vTaskDelay(KeyPressTime / portTICK_PERIOD_MS);

    // release keys
    HID_buffer[0] = 0;
    HID_buffer[2] = 0;
    USBD_HID_SendReport(&hUsbDeviceFS, HID_buffer, 8);
}

void USB_Keyboard_DecodeChar(const char ch)
{

    // Check if lower or upper case
    if (ch >= 'a' && ch <= 'z')
    {
        // convert ch to HID letter, starting at a = 4
        HID_buffer[2] = (uint8_t)(4 + (ch - 'a'));
    }
    else if (ch >= 'A' && ch <= 'Z')
    {
        // Add left shift
        HID_buffer[0] = HID_buffer[0] | 2;
        // convert ch to lower case
        char newch = ch - ('A' - 'a');
        // convert ch to HID letter, starting at a = 4
        HID_buffer[2] = (uint8_t)(4 + (newch - 'a'));
    }
    else
    {
        printf("Keyboard.c\tNot a letter");
        // not a letter
        return;
    }
}

//  chars[0] special keys (cntrl, alt)
//  chars[1] key (a, e, r)
/*
Speial keys
Bit KEY
0 LEFT CTRL
1 LEFT SHIFT
2 LEFT ALT
3 LEFT GUI
4 RIGHT CTRL
5 RIGHT SHIFT
6 RIGHT ALT
7 RIGHT GUI
*/

void SendChar(const char *chars, uint16_t len)
{
    if (len == 0 || len > 2)
    {
        // to low info
        printf("Keyboard.c\tBad request");
        return;
    }

    uint8_t modifier = (uint8_t) (chars[0] - '0');
    HID_buffer[0] = modifier;

    if(len != 1)
    {
        // modifier + key (ex: shit + r)
        USB_Keyboard_DecodeChar(chars[1]);        
    }
    //only modifier like win itp      
    USB_Keyboard_SendChar();
}