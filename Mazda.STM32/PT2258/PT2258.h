#ifndef _PT2258_H
#define _PT2258_H

#include <stdint.h>
#include <stdbool.h>

// Channels
// Function bits table at PT2258 documentation

#define CHANNEL1_VOLUME_STEP_01 0x90
#define CHANNEL1_VOLUME_STEP_10 0x80
#define CHANNEL2_VOLUME_STEP_01 0x50
#define CHANNEL2_VOLUME_STEP_10 0x40
#define CHANNEL3_VOLUME_STEP_01 0x10
#define CHANNEL3_VOLUME_STEP_10 0x00
#define CHANNEL4_VOLUME_STEP_01 0x30
#define CHANNEL4_VOLUME_STEP_10 0x20
#define CHANNEL5_VOLUME_STEP_01 0x70
#define CHANNEL5_VOLUME_STEP_10 0x60
#define CHANNEL6_VOLUME_STEP_01 0xb0
#define CHANNEL6_VOLUME_STEP_10 0xa0
#define ALL_CHANNELS_VOLUME_1STEP 0xe0
#define ALL_CHANNELS_VOLUME_10STEP 0xd0

#define MUTE 0xF8

// Clear register “C0H” value
#define CLEAR_REGISTER 0xc0

#define INIT_VOLUME 79 // init volume -79dB

// Sending function prototype
void pt2258_send(uint16_t DevAddress, uint8_t *pData, uint16_t Size);
void pt2258_delay(uint16_t miliseconds);

// Init routine of PT2258
void PT2258_init(uint16_t DevAddres);
// Set mute
void PT2258_setMute(bool Mute);
// Set channel volume, attenuation range : 0 to 79dB
void PT2258_setChannelVolume(uint8_t vol, uint8_t chno);
// Set all channels volume, attenuation range : 0 to 79dB
void PT2258_setAllVolume(uint8_t vol);

#endif
