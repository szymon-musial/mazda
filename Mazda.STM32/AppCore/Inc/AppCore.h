
#ifndef __APPCORE_H
#define __APPCORE_H

#ifdef __cplusplus
extern "C" {
#endif

#include "mqtt.h"

void BeforeSystemStartCallback();

void ConnectTaskFun(void *argument);

void mqtt_incoming_data_cb(void *arg, const u8_t *data, u16_t len, u8_t flags);
void mqtt_incoming_publish_cb(void *arg, const char *topic, u32_t tot_len);
void mqtt_connection_cb(mqtt_client_t *client, void *arg, mqtt_connection_status_t status);

void PrintAddress(ip4_addr_t *ipv4);
void SubscribeAll(mqtt_client_t *client, void *arg);

typedef void (*const TaskInitFunction_t)();
void AppCreateTasks();

#ifdef __cplusplus
}
#endif

#endif