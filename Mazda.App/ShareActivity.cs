﻿using Android.App;
using Android.Content;
using Android.OS;
using Android.Widget;
using AndroidX.AppCompat.App;
using Mazda.Shared;
using System;
using System.Net;

namespace Mazda.App
{
    [Activity(Label = "ShareActivity", MainLauncher = false, NoHistory = true, Exported = true)]
    [IntentFilter(new[] { Intent.ActionSend }, Label = "Wyślij do auta", Categories = new[] { Intent.CategoryDefault }, DataMimeTypes = new[] { "text/*"/*, "/"*/ })]

    public class ShareActivity : AppCompatActivity
    {
        MqttClient mqttClient;

        protected override void OnCreate(Bundle savedInstanceState)
        {
            base.OnCreate(savedInstanceState);
            SetContentView(Resource.Layout.share_activity);

            var mqtt_ip = PreferenceManagerUtils.GetSavedIp(BaseContext);
            if (mqtt_ip > 0)
            {
                MqttClient.ConfigureMqttClient(new IPAddress(mqtt_ip).ToString());
            }
            mqttClient = MqttClient.Instance;

            Intent intent = Intent;
            var Data = intent.GetStringExtra("android.intent.extra.TEXT");


            EditText editText1 = FindViewById<EditText>(Resource.Id.editText1);

            editText1.Text = Data;

            Console.WriteLine(intent);

            mqttClient.Send("MapsIntent", Data);
            editText1.Text += "\nDone";
        }
    }
}