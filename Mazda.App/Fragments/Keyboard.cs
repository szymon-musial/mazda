﻿using Android.OS;
using Android.Views;
using Android.Widget;
using AndroidX.Fragment.App;
using Mazda.Shared;
using System.Linq;

namespace Mazda.App.Fragments
{
    public class Keyboard : Fragment
    {
        MqttClient mqttClient;
        EditText drawer;

        public override void OnCreate(Bundle savedInstanceState)
        {
            base.OnCreate(savedInstanceState);
            mqttClient = MqttClient.Instance;
        }

        public override View OnCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState)
        {
            var view = inflater.Inflate(Resource.Layout.fragment_keyboard, container, false);
            drawer = view.FindViewById<EditText>(Resource.Id.AddrEditBox);
            drawer.TextChanged += Drawer_TextChanged;

            return view;
        }


        private void Drawer_TextChanged(object sender, Android.Text.TextChangedEventArgs e)
        {
            try
            {
                var letter = drawer.Text?.Last();
                mqttClient.Send("key", $"0{letter}");
            }
            finally { }
        }
    }
}