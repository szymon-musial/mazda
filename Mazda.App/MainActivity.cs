﻿using System;
using System.Net;
using System.Threading;
using Android.App;
using Android.OS;
using Android.Runtime;
using Android.Views;
using AndroidX.AppCompat.App;
using AndroidX.AppCompat.Widget;
using AndroidX.Core.View;
using AndroidX.DrawerLayout.Widget;
using Google.Android.Material.Color;
using Google.Android.Material.FloatingActionButton;
using Google.Android.Material.Navigation;
using Google.Android.Material.ProgressIndicator;
using Google.Android.Material.Snackbar;
using Mazda.App.Fragments;
using Mazda.Shared;
using Mazda.Shared.AppFragmentMgr;

namespace Mazda.App
{
    [Activity(Label = "@string/app_name", Theme = "@style/AppTheme.NoActionBar", MainLauncher = true)]
    public class MainActivity : AppCompatActivity, NavigationView.IOnNavigationItemSelectedListener
    {
        const string UIThreadName =  "UI Thread";
        public MainActivity()
        {
            if (Thread.CurrentThread.Name != UIThreadName)  //Could not activate JNI Handle -> at System.Threading.Thread.set_Name (System.String value)        
                Thread.CurrentThread.Name = UIThreadName;
                    
        }

        MqttClient mqttClient;
        LinearProgressIndicator linearProgressIndicator;
        DrawerLayout drawer;
        AppFragmentManager appFragmentManager;

        protected override void OnCreate(Bundle savedInstanceState)
        {
            base.OnCreate(savedInstanceState);
            DynamicColors.ApplyToActivitiesIfAvailable(Application);

            Xamarin.Essentials.Platform.Init(this, savedInstanceState);
            SetContentView(Resource.Layout.activity_main);
            Toolbar toolbar = FindViewById<Toolbar>(Resource.Id.toolbar);
            SetSupportActionBar(toolbar);

            appFragmentManager = new AppFragmentManager(MenuItemIDWithFragmentLayout, SupportFragmentManager, Resource.Id.frameLayout);

            FloatingActionButton fab = FindViewById<FloatingActionButton>(Resource.Id.fab);
            fab.Visibility = ViewStates.Invisible;
            fab.Click += FabOnClick;

            drawer = FindViewById<DrawerLayout>(Resource.Id.drawer_layout);
            ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(this, drawer, toolbar, Resource.String.navigation_drawer_open, Resource.String.navigation_drawer_close);
            drawer.AddDrawerListener(toggle);
            toggle.SyncState();

            NavigationView navigationView = FindViewById<NavigationView>(Resource.Id.nav_view);
            navigationView.SetNavigationItemSelectedListener(this);

            linearProgressIndicator = FindViewById<LinearProgressIndicator>(Resource.Id.progress_indicator);

            var mqtt_ip = PreferenceManagerUtils.GetSavedIp(BaseContext);
            if (mqtt_ip > 0)
            {
                MqttClient.ConfigureMqttClient(new IPAddress(mqtt_ip).ToString());
            }

            mqttClient = MqttClient.Instance;
            mqttClient.NotifyEvent += MqttClient_NotifyEvent;
            mqttClient.ConnectionStateChangedEvent += MqttClient_ConnectionStateChangedEvent;

            var menuTransaction = SupportFragmentManager.BeginTransaction();
            menuTransaction.Replace(Resource.Id.frameLayout, new DefaultFragmentInflater(Resource.Layout.fragment_start), "Start");
            menuTransaction.Commit();
        }

        private void MqttClient_ConnectionStateChangedEvent(bool newState)
            => RunOnUiThread(() =>
            {
                if (newState)
                {
                    linearProgressIndicator.Hide();
                }
                else
                {
                    linearProgressIndicator.Show();
                }
            });


        private void MqttClient_NotifyEvent(string message)
            => RunOnUiThread(() => Android.Widget.Toast.MakeText(this, message, Android.Widget.ToastLength.Short).Show());        

        public override void OnBackPressed()
        {
            DrawerLayout drawer = FindViewById<DrawerLayout>(Resource.Id.drawer_layout);
            if (drawer.IsDrawerOpen(GravityCompat.Start))
            {
                drawer.CloseDrawer(GravityCompat.Start);
            }
            else
            {
                base.OnBackPressed();
            }
        }

        public override bool OnCreateOptionsMenu(IMenu menu)
        {
            //MenuInflater.Inflate(Resource.Menu.menu_main, menu);
            return true;
        }

        private void FabOnClick(object sender, EventArgs eventArgs)
        {
            View view = (View)sender;
            Snackbar.Make(view, "Replace with your own action", Snackbar.LengthLong)
                .SetAction("Action", (Android.Views.View.IOnClickListener)null).Show();
        }


        private static readonly MenuConfig[] MenuItemIDWithFragmentLayout = new MenuConfig[]
        {
            new MenuConfig(Resource.Id.navmenu_start, Resource.Layout.fragment_start, "start"),
            new MenuConfig(Resource.Id.navmenu_maps, new MapsSender(), "maps_sender"),
            new MenuConfig(Resource.Id.navmenu_keyboard, new Keyboard(), "keyboard"),
            new MenuConfig(Resource.Id.navmenu_bt, Resource.Layout.fragment_bt, "bt"),
            new MenuConfig(Resource.Id.navmenu_sound_selector, Resource.Layout.fragment_sound_select, "SoundSelect"),
            new MenuConfig(Resource.Id.navmenu_lcd, Resource.Layout.fragment_lcd, "Lcd"),
            new MenuConfig(Resource.Id.navmenu_can, Resource.Layout.fragment_can, "Can"),
            new MenuConfig(Resource.Id.navmenu_gpio, Resource.Layout.fragment_gpio, "Gpio"),
            new MenuConfig(Resource.Id.navmenu_settings, new Settings(), "navmenu_settings"),                       
        };

        public bool OnNavigationItemSelected(IMenuItem item)
        {
            var res = appFragmentManager.UpdateFragmentInFragmentManager(item);
            drawer.CloseDrawer(GravityCompat.Start);
            return res;
        }

        public override void OnRequestPermissionsResult(int requestCode, string[] permissions, [GeneratedEnum] Android.Content.PM.Permission[] grantResults)
        {
            Xamarin.Essentials.Platform.OnRequestPermissionsResult(requestCode, permissions, grantResults);

            base.OnRequestPermissionsResult(requestCode, permissions, grantResults);
        }
    }
}

