#include "BM64.h"
#include <SoftwareSerial.h>
SoftwareSerial _swSerial(2, 3);

#define TX_IND 9
#define BM64_DEBUG

BM64 bm64(Serial, TX_IND);

void onEventCallback(BM64_event_t *event){
    // handle Event
    _swSerial.print("[EVENT]: ");
    _swSerial.println(event->event_code, HEX);

    for(int i=0; i<event->param_len; i++){
        _swSerial.print(event->parameter[i], HEX);
        _swSerial.print(" ");
    }
    _swSerial.println("");
}


void setup() {
    _swSerial.begin(115200);
    Serial.begin(115200);
    bm64.setCallback(onEventCallback);
}

void loop() {
    //Check UART Event
    bm64.run();
    if (_swSerial.available() > 0)
    {
    // read the incoming byte:
    char c = _swSerial.read();
    switch (c)
    {
        case 'a':
            bm64.getStatus();
            break;
        case 'b':
            // Other Action, Please see BM64_Debug.h
            bm64.musicControl(MUSIC_CONTROL_PLAY);
            break;
        case 'c':
            // Other Action, Please see BM64_Debug.h
            bm64.musicControl(MUSIC_CONTROL_NEXT);
            break;
        case 'd':
            bm64.generateTone(0x07);
            break;
        case 'e':
            // Other Action, Please see BM64_Debug.h
            bm64.mmiAction(BM64_MMI_ACCEPT_CALL);
            break;
        case 'f':
            // Other Action, Please see BM64_Debug.h
            bm64.mmiAction(BM64_MMI_MUTE_MIC);
            break;
        case 'g':
            bm64.powerOff();
            break;
        case 'h':
            bm64.powerOn();
            break;
        case 'k':
            bm64.getSongName(0);
            break;
        case 'l':
            bm64.getPhoneName1();
            break;
        case 'i':
        {
            String number_s = "1234567890";
            bm64.makeCall(&number_s);
        }
            break;
        default:
            _swSerial.println(c);
        }
    }
}
