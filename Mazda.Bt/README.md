## Moduł
MPSE: IS2064S_012

## Tryby pracy
MFB, Reset: + +
Przyciski (EAN, P_2):
Normalny: - -
Flash: + +
EEPROM: - +


## Tworzenie EEPROM

Ui Tool generuje pliki konfiguracyjne .txt
Narzędziem MPET należy wybrać default bin file: IS206X_012_DUALMODESPK2.1_E1.0.4.1_1214.bin dla wersji 2.1
Z załadowaniem checkboxa
Następnie oddać dwa pliki:
1. wygenerowany przez ui tool
2. Default_IS206X_012_DUALMODESPK2.1_E1.0_V15.txt (zawiera konfiguracje wewnętrznego kodeka)
Generuje się plik .ipf który wskazuje się w narzędzi eeprom

Moduł jest wrażliwy na napięcie ( działa dobrze na podwyższonym napięciu 3.50V, obniża ryzyko spadku poniżej 3.30V)

## Modyfikacja Pcb

Oryginalna płytata BM64 Daughter Board posiada przysicki w których można zmienić tryby pracy urzadzenia.
W przypadku przełacznika SW1 pozycja EAN posiada nieprawidłowość
Załączenie go nie powoduje oczekiwanego stanu wysokiego ponieważ napięcie w punkcie (4, A) w schemacie [BM 64 Daughter Board](./PCB.pdf) wynosiło w przybliżeniu 1.1V co według dokumentacji producenta znajduje się w stanie nieustalonym (powyżej granicy stanu niskiego oraz poniżej stanu wysokiego)

Dalej proces aktualizacji przebiega wg. linku ponizej

https://microchipsupport.force.com/s/article/BM64-Firmware-update-for-DSPK-v2-1-5-package

## Wysyłanie komendy

HEX: 0xAA 00 <długość wiadomości (po tym) - liczba parametrów > <...parametry> <Suma kontrolna>

```code
// Pauza A2DP
Command: AA 00 03 04 00 06 F3 
```