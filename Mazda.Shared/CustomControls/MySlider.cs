﻿using Android.Content;
using Android.Content.Res;
using Android.Runtime;
using Android.Util;
using Android.Views;
using Android.Widget;
using Google.Android.Material.Slider;
using System;
using System.Linq;
using System.Text.RegularExpressions;

namespace Mazda.Shared.CustomControls
{
    internal class MySlider : CustomControlBase, IBaseOnChangeListener, ILabelFormatter
    {
        #region ctor
        protected MySlider(IntPtr javaReference, JniHandleOwnership transfer) : base(javaReference, transfer)
        {
        }

        public MySlider(Context context) : base(context)
        {
        }

        public MySlider(Context context, IAttributeSet attrs) : base(context, attrs)
        {
        }

        public MySlider(Context context, IAttributeSet attrs, int defStyleAttr) : base(context, attrs, defStyleAttr)
        {
        }

        public MySlider(Context context, IAttributeSet attrs, int defStyleAttr, int defStyleRes) : base(context, attrs, defStyleAttr, defStyleRes)
        {
        }
        #endregion
        TextView title, textView;
        Slider slider;
        string MqttSend = "", MqttSendMessage, LabelFormatter;
        float MaxValue, Step;
        Regex regex;
        int ShownValueStartIndex, SubstractResultFrom;

        float outputValueScale = 1.0f;

        public override void Initialize(Context context, IAttributeSet attrs = null)
        {
            base.Initialize(context, attrs);
            View view = layoutInflater.Inflate(Resource.Layout.mysliderlayout, this, true);

            if (attrs != null)
            {
                TypedArray ta = context.ObtainStyledAttributes(attrs, Resource.Styleable.MySliderStyleable);
                MqttSend = ta.GetString(Resource.Styleable.MySliderStyleable_MqttSend);
                MaxValue = ta.GetFloat(Resource.Styleable.MySliderStyleable_MaxValue, 10);
                Step = ta.GetFloat(Resource.Styleable.MySliderStyleable_Step, 1);
                MqttSendMessage = ta.GetString(Resource.Styleable.MySliderStyleable_MqttSendMessage) ?? "{0}";
                LabelFormatter = ta.GetString(Resource.Styleable.MySliderStyleable_LabelFormatter) ?? "{0}";
                
                var valueRegex = ta.GetString(Resource.Styleable.MySliderStyleable_ValueRegex);
                if(valueRegex != null)
                {
                    regex = new Regex(valueRegex);
                }
                ShownValueStartIndex = ta.GetInteger(Resource.Styleable.MySliderStyleable_ShownValueStartIndex, 0);
                SubstractResultFrom = ta.GetInteger(Resource.Styleable.MySliderStyleable_SubstractResultFrom, 0);
                ta.Recycle();
            }

            slider = view.FindViewById<Slider>(Resource.Id.slider);
            slider.StepSize = Step;
            slider.ValueTo = MaxValue;

            var addOnChangeListenerImplementation = Java.Lang.Class.ForName("com.google.android.material.slider.BaseSlider").GetDeclaredMethods().FirstOrDefault(x => x.Name == "addOnChangeListener");
            addOnChangeListenerImplementation?.Invoke(slider, this); // this is implementing IBaseOnChangeListener

            var setLabelFormatterImplementation = Java.Lang.Class.ForName("com.google.android.material.slider.BaseSlider").GetDeclaredMethods().FirstOrDefault(x => x.Name == "setLabelFormatter");
            setLabelFormatterImplementation?.Invoke(slider, this);

            title = view.FindViewById<TextView>(Resource.Id.title);
            title.Text = CardTitle;

            textView = view.FindViewById<TextView>(Resource.Id.textview);

            outputValueScale = 100 / MaxValue;

        }

        public override void Callback(string payload)
        {            
            if(regex is null)
            {
                if (int.TryParse(payload.Substring(ShownValueStartIndex), out int intValue))
                {
                    activity.RunOnUiThread(() => textView.Text = string.Format(LabelFormatter, ((int)((SubstractResultFrom - intValue) * outputValueScale))));
                    return;
                }

                activity.RunOnUiThread(() => textView.Text = string.Format(LabelFormatter, payload.Substring(ShownValueStartIndex)));
                return;
            }

            if(regex.IsMatch(payload))
            {
                if (int.TryParse(payload.Substring(ShownValueStartIndex), out int intValue))
                {
                    activity.RunOnUiThread(() => textView.Text = string.Format(LabelFormatter, ((int)((SubstractResultFrom - intValue) * outputValueScale))));
                    return;
                }
                activity.RunOnUiThread(() => textView.Text = string.Format(LabelFormatter, payload.Substring(ShownValueStartIndex)));
            }           
        }

        public void OnValueChange(Java.Lang.Object sender, float newValue, bool p2)
        {
            mqttClient.Send(MqttSend, string.Format(MqttSendMessage, newValue) );       
        }

        public string GetFormattedValue(float newValue)
            => string.Format(LabelFormatter, (int)((SubstractResultFrom - newValue)* outputValueScale) );
    }
}