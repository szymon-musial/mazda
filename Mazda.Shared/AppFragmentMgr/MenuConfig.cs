﻿using AndroidX.Fragment.App;

namespace Mazda.Shared.AppFragmentMgr
{
    public class MenuConfig
    {
        public Fragment ViewFragment { get; }
        public string Tag { get; }
        public int CallerId { get; }

        public MenuConfig(int callerId, int fragmentId, string tag)
        {
            ViewFragment = new DefaultFragmentInflater(fragmentId);
            Tag = tag;
            CallerId = callerId;
        }

        public MenuConfig(int callerId, Fragment destFragment, string tag)
        {
            ViewFragment = destFragment;
            Tag = tag;
            CallerId = callerId;
        }
    }
}
