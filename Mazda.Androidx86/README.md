# Modyfikacje systemu android

W katalogach zostały zamieszczone opisy wraz z plikami

# Inne

## Instalacja

Dysk MBR, typu Basic disc. Partycje posiadają system plików ext4 przez co
android jest instalowany nie w plikach obrazu (np. data.img tylko w folderze
data):

-   boot - grub

-   Android - Główny system plików androida

-   ZorinOs - Pomocniczy system linux

## Wirtualna klawiatura

Po wpisaniu fizycznych klawiatur do pliku android nie sprawdza ich występowania
przez co niezależnie czy jest nie płynie na wyświetlanie symulowanej

https://groups.google.com/g/android-x86/c/O27FGXd2Eag

## Natywny GPS

Zainstalowany sterownik w wersji Android 7.1 jest podczas inicjalizacji pobiera
z wartości środowiskowych dane do komunikacji tekstowej. Ponadto system zawiera
sterownik obsługi portów szeregowych usb

Moduł Ublox w formacie NMEA wysyła pozycję na port (tutaj /dev/ttyACM0)

Należy dostarczyć informację o porcie oraz prędkości transmisji do portu
sterownika GPS pośrednio przez "prop"-y np. dopisując do pliku system/build.prop

``` bash
ro.kernel.android.gps ttyACM0
ro.kernel.android.gpsttybaud 9600
```

## Zmiana rozdzielczości

Aplikacja pomocnicza
https://apkcombo.com/resolution-changer-uses-adb/com.draco.resolutionchanger/
Wymaga przyznania uprawnień adb shell pm grant com.draco.resolutionchanger
android.permission.WRITE_SECURE_SETTINGS

Można poprzez terminal zrobić to tak samo wpisując w shellu

``` bash
wm size
```

Do poprawnego działania dedykowanej karty graficznej (nvidia gt 540) należy w pliku \system\etc\init.sh (po zamontowaniu partycji)
w funckji dopisać
```bash
function init_hal_gralloc()
{
    //...
    set_drm_mode
    set_property debug.drm.mode.force 1366x768 //natywna rozdzielczość ekranu podłączonego przez hdmi
}
```
